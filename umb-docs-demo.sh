#!/bin/bash
ls -ltr umbrella_doc_demo_* | tail -n 1 | awk '{print $9}' | xargs  dpkg -i && sleep 6;
cd /opt/umbrella/frontend/sphinx-documents/html/
# export IPproject='grep 'WS_HOST=' /opt/umbrella/umbrella/.env | awk -F"=" '{print $2}''
# j2 ./index.html > index.html
chown -R www-data:www-data index.html
# systemctl restart apache2
systemctl restart nginx
systemctl restart uwsgi