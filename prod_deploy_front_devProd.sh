#!/bin/bash
# ls -ltr sc00_front_devProd_*.deb | tail -n 1 | awk '{print $9}' | xargs  dpkg -i  --force-overwrite
rm /var/lib/dpkg/info/sc00_front*
systemctl stop apache2
rm -fr /var/www/SmartMonitor/* && sleep 6;
ls -ltr sc00_front_devProd_*.deb | tail -n 1 | awk '{print $9}' | xargs  dpkg -i 
chown -R www-data:www-data /var/www/SmartMonitor
systemctl start apache2