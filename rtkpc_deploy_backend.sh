#!/bin/bash
apt remove sc00_backend_rtkpc/now -y && sleep 4
ls -ltr sc00_backend_rtkpc_*.deb | tail -n 1 | awk '{print $9}' | xargs  dpkg -i
cd /opt/
pm2 reload BackEnd