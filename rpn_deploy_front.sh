#!/bin/bash
# ls -ltr sc00_front_rpn_*.deb | tail -n 1 | awk '{print $9}' | xargs  dpkg -i  --force-overwrite
rm /var/lib/dpkg/info/sc00_front*
systemctl stop apache2
rm -fr /var/www/SmartMonitor/*
ls -ltr sc00_front_rpn_*.deb | tail -n 1 | awk '{print $9}' | xargs  dpkg -i && sleep 3
chown -R www-data:www-data /var/www/SmartMonitor
systemctl start apache2