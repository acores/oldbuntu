#!/bin/bash
export IPHost='192.168.209.10'
ls -ltr sc00_agent_front_*.deb | tail -n 1 | awk '{print $9}' | xargs  dpkg -i
cd /opt/SCAgent/scripts/
j2 ./.env-template > /opt/SCAgent/.env
cd /opt/
pm2 reload SCAgent