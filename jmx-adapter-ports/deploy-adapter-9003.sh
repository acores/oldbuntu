#!/bin/bash
systemctl stop jmxadapter3.service
export nameService='jmxadapter3'
export PWDA='/opt/jmxadapter/9003/'
export IP='192.168.209.14'
export jdkVersion='jdk1.8.0_271'
export AdapterPort='9003'
export versAdapter=$(ls -ltr vmware-zabbix-*.jar | tail -n 1 | awk '{print $9}')

j2 ./TemplateGenServices.sh > /etc/systemd/system/jmxadapter3.service
systemctl daemon-reload
systemctl start jmxadapter3.service