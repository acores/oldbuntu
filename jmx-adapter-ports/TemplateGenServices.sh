#!/bin/bash
#Template-gen vmware-adapter-sbnd.service
[Unit]
Description=Custom {{ nameService }} service
After=network.target

[Service]
WorkingDirectory={{ PWDA }}
Type=simple
Restart=on-failure
RestartSec=10
ExecStart=/opt/jvm/jdk1.8.0_271/bin/java -Dcom.sun.management.jmxremote=true -Dcom.sun.management.jmxremote.port={{ AdapterPort }} -Dcom.sun.management.jmxremote.rmi.port={{ AdapterPort }} -Dcom.sun.management.jmxremote.ssl=false -Dcom.sun.management.jmxremote.authenticate=false -Djava.rmi.server.hostname={{ IP }} -jar {{ PWDA }}{{ versAdapter  }} --tplg_flat

[Install]
WantedBy=multi-user.target
