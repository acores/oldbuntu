#!/bin/bash
systemctl stop jmxadapter1.service
export nameService='jmxadapter1'
export PWDA='/opt/jmxadapter/9001/'
export IP='192.168.209.14'
export jdkVersion='jdk1.8.0_271'
export AdapterPort='9001'
export versAdapter=$(ls -ltr vmware-zabbix-*.jar | tail -n 1 | awk '{print $9}')

j2 ./TemplateGenServices.sh > /etc/systemd/system/jmxadapter1.service
systemctl daemon-reload
systemctl start jmxadapter1.service