#!/bin/bash
systemctl stop jmxadapter6.service
export nameService='jmxadapter6'
export PWDA='/opt/jmxadapter/9006/'
export IP='192.168.209.14'
export jdkVersion='jdk1.8.0_271'
export AdapterPort='9006'
export versAdapter=$(ls -ltr vmware-zabbix-*.jar | tail -n 1 | awk '{print $9}')

j2 ./TemplateGenServices.sh > /etc/systemd/system/jmxadapter6.service
systemctl daemon-reload
systemctl start jmxadapter6.service