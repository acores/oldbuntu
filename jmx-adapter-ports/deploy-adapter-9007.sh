#!/bin/bash
systemctl start jmxadapter7.service
export nameService='jmxadapter7'
export PWDA='/opt/jmxadapter/9007/'
export IP='192.168.209.14'
export jdkVersion='jdk1.8.0_271'
export AdapterPort='9007'
export versAdapter=$(ls -ltr vmware-zabbix-*.jar | tail -n 1 | awk '{print $9}')

j2 ./TemplateGenServices.sh > /etc/systemd/system/jmxadapter7.service
systemctl daemon-reload
systemctl start jmxadapter7.service