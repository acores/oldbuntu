#!/bin/bash
systemctl stop jmxadapter5.service
export nameService='jmxadapter5'
export PWDA='/opt/jmxadapter/9005/'
export IP='192.168.209.14'
export jdkVersion='jdk1.8.0_271'
export AdapterPort='9005'
export versAdapter=$(ls -ltr vmware-zabbix-*.jar | tail -n 1 | awk '{print $9}')

j2 ./TemplateGenServices.sh > /etc/systemd/system/jmxadapter5.service
systemctl daemon-reload
systemctl start jmxadapter5.service