#!/bin/bash

ls -ltr front_umbrella_DEV_* | tail -n 1 | awk '{print $9}' | xargs  dpkg -i && sleep 6;
# cd /opt/umbrella/frontend/
# export link='https://192.168.200.200:8080/api/'
# export type='DEV'
# export webVersion=$(awk '/v2/{print $0}' config.js)
# j2 ./scripts/template-config.js > config.js
chown -R www-data:www-data /opt/umbrella/frontend/config.js
systemctl restart nginx
systemctl restart uwsgi