#!/bin/bash

ls -ltr front_umbrella_* | tail -n 1 | awk '{print $9}' | xargs  dpkg -i && sleep 6;
cd /opt/frontend/
export link='https://111.111.111.test-1:8080/api/'
j2 ./config.js > config-j2.js && sleep 2 && rm ./config.js && mv config-j2.js config.js
chown -R www-data:www-data /opt/frontend/config.js
systemctl restart apache2
