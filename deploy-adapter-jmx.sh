#!/bin/bash
export nameService='jmxadapter'
export PWDA='/opt/jvm/'
export IP='192.168.209.14'
export jdkVersion='jdk1.8.0_271'
export versAdapter=$(ls -ltr vmware-zabbix-*.jar | tail -n 1 | awk '{print $9}')

j2 ./TemplateGenServices.sh > /etc/systemd/system/jmxadapter.service

systemctl deamon-reload
systemctl enable jmxadapter.service
systemctl start jmxadapter.service && sleep 3;
systemctl status jmxadapter.service