#!/bin/bash

ls -ltr front_umbrella_* | tail -n 1 | awk '{print $9}' | xargs  dpkg -i && sleep 6;
cd /opt/umbrella/frontend/
export link='https://10.182.100.59:8080/api/'
rm config.js
j2 ./config-j2.js > config-j.js && sleep 2 && rm ./config-j2.js && mv config-j.js config.js
chown -R www-data:www-data /opt/umbrella/frontend/config.js
systemctl restart apache2
